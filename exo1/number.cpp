#include <iostream>
using namespace std;

int main()
{
    int  numberOfNote;
    cout << "Enter N" << endl;
    cin >> numberOfNote;

    while (numberOfNote <= 0)
    {
        cout << "You must enter a positive number" << endl;
        cin >> numberOfNote;
    }

    int number;
    double tableau[numberOfNote];
    cout << "Enter numbers : " << endl;
     
     for (int i = 0; i < numberOfNote; i++)
     {
        cin >> tableau[i];
     }


    double moyenne;
    double somme;
     for (int i = 0; i < numberOfNote; i++)
     {
        somme += tableau[i];
     }

     moyenne = somme / numberOfNote;

     double max;
     max = tableau[0];
    for (int i = 0; i < numberOfNote; i++)
    {
        if (max < tableau[i])
        {
            max = tableau[i];
        }
    }

    double min;
     min = tableau[0];
    for (int i = 0; i < numberOfNote; i++)
    {
        if (min > tableau[i])
        {
            min = tableau[i];
        }
    }


    cout << "Mean : " << moyenne << endl;
    cout << "Max : " << max << endl;
    cout << "Min : " << min << endl;
    cout << "Range : " << max - min << endl;

    return 0;
}